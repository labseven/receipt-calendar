from calendar import *
from datetime import date
import argparse

parser = argparse.ArgumentParser('Print ascii calendars on receipt printers')

parser.add_argument('--port', default='/dev/usb/lp1',
                    help='printer port (/dev/usb/lp1)')
parser.add_argument('-m', '--month', type=int, default=date.today().month)
parser.add_argument('-y', '--year', type=int, default=date.today().year)
parser.add_argument('-w', '--width', type=int, default=50,
                    help="paper width in chars")
parser.add_argument('-d', '--dry', action='store_true',
                    help='dry run (only print to terminal)')

args = parser.parse_args()


def printOut(lines):
    if args.dry:
        for line in range(2):
            print('|' + ' ' * args.width + '|')

        for line in lines:
            line = '|' + line[:args.width].center(args.width) + '|'
            print(line)

        for line in range(2):
            print('|' + ' ' * args.width + '|')
    else:
        with open(args.port, 'w') as printer:
            for line in lines:
                printer.write(
                    '  ' + line[:args.width].center(args.width) + '\n')
            for _ in range(10):
                printer.write('\n')


class myTextCalendar(TextCalendar):
    def formatday(self, day, weekday, width):
        if day == 0:
            s = '|' + ' '*(width-2)
        else:
            # s = '%2i' % day             # right-align single-digit days
            # s = '| %2i' % day + ' '*(width-5)
            s = '|' + ' '*(width-4) + '%2i' % day
        # return '|' + s.center(width - 2)
        return s


def myFormatMonth(theyear, themonth):
    tc = myTextCalendar()

    dayW = (args.width-1)//7
    pageW = (dayW * 7) + 1
    dayH = 3

    s = []
    s.append(tc.formatmonthname(theyear, themonth, pageW))
    s = s + ['']*2
    s.append(tc.formatweekheader(dayW-1))
    s.append('-'*pageW)
    for week in tc.monthdays2calendar(theyear, themonth):
        s.append(tc.formatweek(week, dayW) + ' |')
        s = s + [('|' + ' '*(dayW-1))*7 + '|']*(dayH - 1)
        s.append('-'*pageW)

    return s


printOut(myFormatMonth(args.year, args.month))
