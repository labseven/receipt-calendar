# Receipt Calendar

Prints an ascii calendar on a receipt printer.

```
usage: Print ascii calendars on receipt printers [-h] [--port PORT] [-m MONTH] [-y YEAR] [-w WIDTH] [-d]

optional arguments:
  -h, --help            show this help message and exit
  --port PORT           printer port (/dev/usb/lp1)
  -m MONTH, --month MONTH
  -y YEAR, --year YEAR
  -w WIDTH, --width WIDTH
                        paper width in chars
  -d, --dry             dry run (only print to terminal)
```

![february printed](feb.jpg "Feb 2021")